------
Django project template
------



Installation
------------

1. Create working virtual environment
2. Install django::

    pip install django

3. Create new project from this template::

    django-admin.py startproject --template=https://bitbucket.org/Somebuddy/django-project-template/get/master.zip --extension=py,rst, html django_test

4. Install requirements

    * for development server::

        pip install -r requirements/local.txt

    * for production server::

        pip install -r requirements.txt

5. Set environment variables

    * Add project directory into PYTHONPATH::

        set "PYTHONPATH=d:\workspace\git\dtest\django_test\django_test;%PYTHONPATH%"

    * SECRET_KEY::

        set "SECRET_KEY={{ your_secret_key }}"

    * DEFAULT_DATABASE

        * postgres::

            set "DATABASE_URL=postgres://user:password@host/database"

        * sqlite::

            set "DATABASE_URL=sqlite:////path-to-your-database-file"

    * DJANGO_SETTINGS_MODULE

        * for development server::

            set "DJANGO_SETTINGS_MODULE=django_test.settings.local"

        * for production server::

            set "DJANGO_SETTINGS_MODULE=django_test.settings.local"


Installed Packages
------------------

    * base
        Django==1.6.2
        South==0.8.4
        dj-database-url==0.2.2

    * development server
        django-debug-toolbar==1.0.1
        sqlparse==0.1.10
        coverage==3.7.1
        docutils==0.11


