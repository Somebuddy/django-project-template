from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.conf import settings

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', '{{ project_name }}.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', TemplateView.as_view(template_name="base.html"), name="index"),
    url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    # enable debug toolbar
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^debug/', include(debug_toolbar.urls)),
    )

    # enable static
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
